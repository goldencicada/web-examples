import { useNavigate } from 'react-router-dom';
import './styles.css';

const Header = () => {
  const navigate = useNavigate();
  const viewNavigate = (newRoute) => {
    if (!document.startViewTransition) {
      return navigate(newRoute);
    } else {
      return document.startViewTransition(() => {
        navigate(newRoute);
      });
    }
  }

  return (
    <div className="header__container">
      <span>ReactJs - React Router - View Transitions API</span>
      <div className="link__container">
        <button onClick={() => viewNavigate('/view-transition')}>Home</button>
        <button onClick={() => viewNavigate('/view-transition/download')}>Download</button>
        <button onClick={() => viewNavigate('/view-transition/about')}>About</button>
      </div>
    </div>
  )
};

export default Header;