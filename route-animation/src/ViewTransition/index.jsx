import { useMemo } from 'react';
import { Routes, Route } from 'react-router-dom';
import './styles.css';

import Home from '../Home';
import Header from './Header';
import PageNotFound from '../PageNotFound';
import About from '../About';
import Download from '../Download';

function ViewTransition() {
  const isViewTransition = useMemo(() => {
    if (document.startViewTransition) {
      return "Yess, Your browser support View Transitions API";
    }
    return "Opss, Your browser doesn't support View Transitions API";
  }, []);

  return (
    <>
      <Header />
      <Routes>
        <Route index element={<Home />} />
        <Route path="about" element={<About />} />
        <Route path="download" element={<Download />} />
        <Route path="*" element={<PageNotFound />} />
      </Routes>
      <footer>
        <a href="/">Home</a>
        <p>{isViewTransition}</p>
      </footer>
    </>
  );
}

export default ViewTransition;
