import { Routes, Route, useLocation } from 'react-router-dom';
import { motion, AnimatePresence } from 'framer-motion';

import Header from './Header';
import Home from '../Home';
import PageNotFound from '../PageNotFound';
import About from '../About';
import Download from '../Download';

// const motionConfig = {
//   initial: { y: -20, opacity: 0 },
//   animate: { y: 0, opacity: 1 },
//   transition: {duration: 0.7,ease: [0.6, -0.05, 0.01, 0.99]},
// };

const motionConfig = {
  initial: { width: 0 },
  animate: { width: '100%' },
  exit: { x: '100%', opacity: 0 },
  // transition: { duration: 2 },
  transition: { duration: 2, type: "spring", stiffness: 100 },
};

const MotionPageWrapper = ({ children }) => (
  <motion.main className="main__container" {...motionConfig}>
    {children}
  </motion.main>
)

const FramerMotion = () => {
  const location = useLocation();

  return (
    <>
      <Header />
      <AnimatePresence initial={false} mode="wait">
        <Routes location={location} key={location.pathname}>
          <Route index element={<MotionPageWrapper><Home /></MotionPageWrapper>} />
          <Route path="about" element={<MotionPageWrapper><About /></MotionPageWrapper>} />
          <Route path="download" element={<MotionPageWrapper><Download /></MotionPageWrapper>} />
          <Route path="*" element={<MotionPageWrapper><PageNotFound /></MotionPageWrapper>} />
        </Routes>
      </AnimatePresence>
      <footer>
        <a href="/">Home</a>
      </footer>
    </>
  );
}

export default FramerMotion;