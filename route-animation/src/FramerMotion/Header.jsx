import { Link } from 'react-router-dom';

const Header = () => (
  <div className="header__container">
    <span>ReactJs - React Router - Framer Motion</span>
    <div className="link__container">
      <Link to="/framer-motion">Home</Link>
      <Link to="/framer-motion/download">Download</Link>
      <Link to="/framer-motion/about">About</Link>
    </div>
  </div>
);

export default Header;