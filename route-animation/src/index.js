import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import App from './App';
import FramerMotion from './FramerMotion';
import ViewTransition from './ViewTransition';
import PageNotFound from './PageNotFound';

import './index.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route index element={<App />} />
        <Route path="/framer-motion/*" element={<FramerMotion />} />
        <Route path="/view-transition/*" element={<ViewTransition />} />
        <Route path="/*" element={<PageNotFound />} />
      </Routes>
    </BrowserRouter>
  </React.StrictMode>
);
