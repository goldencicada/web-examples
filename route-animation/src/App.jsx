import { Link } from 'react-router-dom';

const App = () => (
  <div className="link__container">
    <Link to="/framer-motion">FramerMotion</Link>
    <Link to="/view-transition">ViewTransition</Link>
  </div>
)

export default App;