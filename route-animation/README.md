This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

1. [Framer Motion Route Animation](./src/FramerMotion)
2. [View Transition API Animation](./src/ViewTransition)